# PHP Calendar #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Showing events in a seasonal plan
* v0.0.3

### How do I get set up? ###

* Require class file into your project.
* Initiallize the project with $calendar = CalendarHelper::getInstance();
* Output the calendar with echo $calendar->render();

### Example of usage ###
~~~~
<?php
	
	// Requirering the class
	require_once "app/calendar.php";
	
	// Initiallize the helper class
	$calendar = Calendar::getInstance();
	
	// Adding events by setting an array
	$calendar->events = [
		'2017-12-24' => [
			[
				'date' => '2017-12-24',
				'name' => 'Juleaften'
			]
		],
		'2017-12-25' => [
			[
				'date' => '2017-12-25',
				'name' => '1. Juledag'
			]
		],
		'2017-12-26' => [
			[
				'date' => '2017-12-26',
				'name' => '2. Juledag'
			]
		],
		'2017-12-31' => [
			[
				'date' => '2017-12-31',
				'name' => 'Nytårsaften'
			]
		]
	];
	
	// Output by echo render();
	$calendar->render();
	
~~~~
