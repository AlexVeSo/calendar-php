<?php 
require_once "app/calendar.php";

$calendar = CalendarHelper::getInstance();

$calendar->events = [
    '2017-12-24' => [
        [
            'date' => '2017-12-24',
            'name' => 'Juleaften'
        ]
    ],
    '2017-12-25' => [
        [
            'date' => '2017-12-25',
            'name' => '1. Juledag'
        ]
    ],
    '2017-12-26' => [
        [
            'date' => '2017-12-26',
            'name' => '2. Juledag'
        ]
    ],
    '2017-12-31' => [
        [
            'date' => '2017-12-31',
            'name' => 'Nytårsaften'
        ]
    ]
];
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calendar 0.0.3</title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    
    <style>
        #calendar.table-condensed>tbody>tr>td, 
        #calendar.table-condensed>tbody>tr>th, 
        #calendar.table-condensed>tfoot>tr>td, 
        #calendar.table-condensed>tfoot>tr>th {
            padding: 0px;
        }
        #calendar span {
            display: inline-block;
        } 
        #calendar span.left {
            width: 33px;
            float: left;
            padding: 5px;
        }
        #calendar span.right {
            width: calc(100% - 33px);
            float: right;
            padding: 5px;
            text-align: left;
        }
        #calendar span.right .badge {
            float: right;
        }
        #calendar .sunday {
            background-color: lightgray;
        }
        #calendar .saturday span.left {
            background-color: lightgray;
        }
    </style>
</head>
<body>
    
    <div class="container">
        <h2>Calendar app</h2>
        <?php echo $calendar->render(true); ?>
    </div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>