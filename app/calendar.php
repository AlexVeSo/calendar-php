<?php
//defined('_JEXEC') or die('Restricted access');

require_once "mobileDetect.php";

class CalendarHelper extends MobileDetect {
	
	private static $_instance = null;
	public $_events,
		   $_dateFrom,
		   $_dateTo,
		   $_year,
		   $_prev = '',
		   $_next = '',
		   $_url = '',
           $_isMobile;
	
	public function __construct(){
		/*$app	= JFactory::getApplication();
		
		$season	= $app->input->get('season', $this->getSeason(date('m'))); //$this->getSeason($app->input->get('season', date('m')));
		$year	= $app->input->get('year', date('Y'));*/
        
        $season = isset($_GET['season'])? $_GET['season']: $this->getSeason(date('m'));
        $year   = isset($_GET['year'])? $_GET['year']: date('Y');
        
		$this->_year = $year;

		$this->_url = "index.php"; //?option={$app->input->get('option')}&view={$app->input->get('view')}&Itemid={$app->input->get('Itemid')}
        $this->_isMobile = self::detectMobile();
		switch ($season) {
			case 1:
				$monthFrom = 1;
				$monthTo = 6;
				$this->_prev = $this->_url . "?season=2&year=" . ((int)$year - 1);
				$this->_next = $this->_url . "?season=2&year=" . (int)$year;
				break;
			case 2:
				$monthFrom = 7;
				$monthTo = 12;
				$this->_prev = $this->_url . "?season=1&year=" . (int)$year;
				$this->_next = $this->_url . "?season=1&year=" . ((int)$year + 1);
				break;
		}
		
		$this->_dateFrom = date('Y-m-d', strtotime($year.'-'.$monthFrom.'-1'));
		$this->_dateTo = date('Y-m-t', strtotime($year.'-'.$monthTo));
	}
	
	public static function getInstance()
	{
		if (!isset(self::$_instance)) {
			self::$_instance = new CalendarHelper();
		}
		return self::$_instance;
	}
	
	public function render($isMobile = false)
	{
        if (!$isMobile) {
            $this->_isMobile = $isMobile;
        }
		$months   = $this->generateMonths($this->_dateFrom, $this->_dateTo);
		$days     = $this->generateDays($months);        
        $template = $this->generateTemplate($days, $this->_isMobile);

		return $template;
	}
	
	public function generateMonths($fromDate, $toDate)
	{
		$months 	= [];
		
		$start 		= new DateTime($fromDate);
		$end 		= new DateTime($toDate);
		$interval	= DateInterval::createFromDateString('1 month');
		$period		= new DatePeriod($start, $interval, $end);
		
		foreach ($period as $dt) {
			$months[] = [
				'month' => $dt->format("m"),
				'monthname' => date("F", strtotime($dt->format("Y-m-01"))),//JFactory::getDate($dt->format("Y-m-01"))->format('F'), //F
				'daysInMonth' => $dt->format("t")
			];
		}
		
		return $months;
	}
	
	public function generateDays($months)
	{
		$return = [];
		$return['months'] = $months;
		$events = $this->_events['events'];
        
        foreach ($months as $key => $month) {
			for ($i = 1; 31 >= $i; $i++) {
				if($month['daysInMonth'] >= $i){
                    $return['days'][$i][$key]['isDay']         = true;
					$return['days'][$i][$key]['dayname']       = date('D', strtotime("{$this->_year}-{$month['month']}-{$i}")); //->format('D')
					$return['days'][$i][$key]['daynameorg']    = date('D', strtotime("{$this->_year}-{$month['month']}-{$i}")); //JFactory::getDate()->format('D');
					$return['days'][$i][$key]['date']          = date('Y-m-d', strtotime("{$this->_year}-{$month['month']}-{$i}")); //JFactory::getDate()->format('D');

					$day = (strlen($i) == 1)? "0".$i: (string)$i;

					$searchKey = $this->_year.'-'.$month['month'].'-'.$day;

					if (!is_null($events[$searchKey])) {
						$return['days'][$i][$key]['events'] = $events[$searchKey];
					}
				} else {
					$return['days'][$i][$key]['isDay'] = false;
				}
			}
		}

		//echo '<pre>',var_dump($return),'</pre>'; exit;
		/*$events = $this->_events['events'];
		//echo '<pre>',var_dump($events),'</pre>';
		$k = 0;
		$return = new stdClass;
		foreach($months as $key => $month) {
			$return->{$k}->month = $month;
			$daysInMonth = date('t', strtotime($this->_year.'-'.$month.'-1'));
			for ($i = 1; $i <= $daysInMonth; $i++) {
				$return->{$k}->days->{$i}->dayname = date('D', strtotime($this->_year.'-'.$month.'-'.$i));
				$searchKey = $this->_year.'-'.$month.'-'.$i;
				//var_dump($events[$searchKey]);
				if (!is_null($events[$searchKey])) {
					$return->{$k}->days->{$i}->events = $events[$searchKey];
				}
			}
			$k++;
		}*/
		return $return;
	}
    
    public function generateTemplate($data = [], $isMobile)
    {
        if (!$isMobile) {
            $output = "<table class='table table-bordered table-condensed' id='calendar'>";        
            $output .= "<thead>";
            $output .= "<tr>";
            $output .= "<th>{$this->getPrev()}</th>";
            $output .= "<th colspan='4' style='text-align: center;'>{$this->getYear()}</th>";
            $output .= "<th style='text-align: right;'>{$this->getNext()}</th>";
            $output .= "</tr>";
            $output .= "</thead>";
            $output .= "<thead>";
            $output .= "<tr>";
            foreach ($data['months'] as $month) {
                $output .= "<th width='16.6666666667%' style='text-align: center;'>" . ucfirst($month['monthname']) . "</th>";
            }
            $output .= "</tr>";
            $output .= "</thead>";
            foreach ($data['days'] as $day => $info) {
                $output .= "<tr>";
                foreach ($info as $item) {

                    $class = '';
                    if ($item['daynameorg'] == 'Sun')
                        $class .= ' sunday';
                    else if ($item['daynameorg'] == 'Sat')
                        $class .= ' saturday';

                    $output .= "<td class='{$class}'>";
                    if ($item['isDay']) {
                        $output .= "<span class='left'>" . substr($item['dayname'], 0, 1) . $day . "</span>";
                        $output .= "<span class='right'>";
                        if (isset($item['events'])) {
                            if (count($item['events']) == 1) {
                                $event = (isset($item['events'][0]['link']))? "<a href='{$item['events'][0]['link']}'>{$item['events'][0]['name']}</a>": $item['events'][0]['name'];
                                $output .= "<span class='event'>{$event}</span>";
                            } else {
                                $output .= "<span class='event'>" . count($item['events']) . " Events funcet</span>";
                            }                            
                        }
                        if ($item['daynameorg'] == 'Mon')
                            $output .= "<span class='badge'>" . ltrim(date('W', strtotime($item['date'])), '0') . "</span>";
                        $output .= "</span>";
                    }                    
                    $output .= "</td>";
                }
                $output .= "</tr>";
            }        
            $output .= "</table>";   
        }
        return $output;
    }

	public function getPrev()
	{
		return "<a href='{$this->_prev}'>Forrige</a>"; //JText::_('JPrev')
	}

	public function getNext()
	{
		return "<a href='{$this->_next}'>Næste</a>"; //JText::_('JNext')
	}

	public function getYear()
	{
		return $this->_year;
	}
	
	public function getSeason($month)
	{
		return ($month <= 6)? 1: 2;
	}
		
	public function __get($name)
	{
		return $this->_events[$name];
	}
	
	public function __set($name, $value)
	{
		$this->_events[$name] = $value;
	}	
}